<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserVacationController;
use App\Http\Controllers\QueueController;
use App\Http\Controllers\QueueHistoryController;
use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
#CLIENTS
Route::get('/clients', [ClientController::class, 'index']);

Route::get('/clients/{id}', [ClientController::class, 'show']);

Route::post('/clients', [ClientController::class, 'store']);

Route::put('/clients/{id}', [ClientController::class, 'update']);

Route::delete('/clients/{id}', [ClientController::class, 'destroy']);

#PRODUTOS
Route::get('/products', [ProductController::class, 'index']);

Route::get('/products/{id}', [ProductController::class, 'show']);

Route::post('/products', [ProductController::class, 'store']);

Route::put('/products/{id}', [ProductController::class, 'update']);

Route::delete('/products/{id}', [ProductController::class, 'destroy']);

#QUEUE
Route::get('/queue', [QueueController::class, 'index']);

Route::get('/queue/{id}', [QueueController::class, 'show']);

Route::post('/queue', [QueueController::class, 'store']);

Route::put('/queue/{id}', [QueueController::class, 'update']);

Route::delete('/queue/{id}', [QueueController::class, 'destroy']);

#QUEUE HISTORY
Route::get('/queue-history', [QueueHistoryController::class, 'index']);

Route::get('/queue-history/{id}', [QueueHistoryController::class, 'show']);

Route::post('/queue-history', [QueueHistoryController::class, 'store']);

Route::put('/queue-history/{id}', [QueueHistoryController::class, 'update']);

Route::delete('/queue-history/{id}', [QueueHistoryController::class, 'destroy']);

#USERS
Route::post('/users/auth', [UserController::class, 'login']);

Route::get('/users/auth/logout', [UserController::class, 'logout']);

Route::get('/users', [UserController::class, 'index']);

Route::get('/users/auth/', [UserController::class, 'verify_login']);

Route::get('/users/{id}', [UserController::class, 'show']);

Route::put('/users/{id}', [UserController::class, 'update']);

Route::delete('/users/{id}', [UserController::class, 'destroy']);

#USER VACATION

Route::get('/users-vacation', [UserVacationController::class, 'index']);

Route::get('/users-vacation/{id}', [UserVacationController::class, 'show']);

Route::post('/users-vacation', [UserVacationController::class, 'store']);

Route::put('/users-vacation/{id}', [UserVacationController::class, 'update']);

Route::delete('/users-vacation/{id}', [UserVacationController::class, 'destroy']);



