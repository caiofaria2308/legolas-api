<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\Client;

class ClientController extends Controller
{
    /**
     * Decrypt token and return the data.
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function token_decrypt(Request $request){
        $token = $request->bearerToken(); 
        if($token == null){
            return [
                "status" => false, 
                "message" =>"token é obrigatório!"
            ];
        }
        $token_decrypt = Crypt::decrypt($token);
        $token = explode(',', $token_decrypt);        
        $authToken = $token[0];
        $userId = $token[1];
        $isadmin = $token[2];
        $header = [
            "X-Auth-Token" => $authToken,
            "X-User-Id" => $userId
        ];
        try{
            $url = 'https://chat.interativanet.com.br/api/v1/me';
            $response = Http::withHeaders($header)->get($url);
            $response = $response -> json();
            if($response["status"] != "error"){
                return [
                    "status" => true,
                    "authToken"=>$authToken,
                    "userId"=>$userId,
                    "isadmin"=>$isadmin
                ];
            }else{
                return [
                    "status" => false
                ];
            }
        }catch(Exception $e){
            return [
                "status" => false
            ];
        }
    }

    function formatString($string) {

        // matriz de entrada
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç','.','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º');
    
        // matriz de saída
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','','','','','','','','','','','','','','','','','','','','','','','' );
    
        // devolver a string
        try{
            return str_replace($what, $by, $string);
        }catch(Exception){
            return $string;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get all client
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message"=> "token inválido ou expirado"
            ];
        }
        try{
            $clients = Client::orderBy("fantasy_name")->get();
            return [
                "status" => true,
                "clients" => $clients
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao selecionar clientes"
            ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store new client
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message"=> "token inválido ou expirado"
            ];
        }
        $validate = $request->validate([
            "_id_crm" => ["required", "string"],
            "business_name" => ["required", "string"],
            "fantasy_name" => ["required", "string"],
            "phone" => ["nullable", "string"],
            "phone2" => ["nullable", "string"],
            "document_primary" => ["nullable", "string"],
            "document_secundary" => ["nullable", "string"],
            "zip_code" => ["nullable", "string"],
            "address" => ["nullable", "string"],
            "address_number" => ["nullable", "string"],
            "district" => ["nullable", "string"],
            "city" => ["nullable", "string"],
            "state" => ["nullable", "string"],
            "is_active" => ["required", "boolean"],
        ]);
        if(!$validate){
            return [
                "status" => false,
                "message" => "campos obrigátorios não preenchidos"
            ];
        }
        try {
            $client = new Client;
            $client->_id_crm = $request->_id_crm;
            $client->business_name = $request->business_name;
            $client->fantasy_name = $request->fantasy_name;
            $client->phone = $request->phone;
            $client->phone_2 = $request->phone_2;
            $client->document_primary = $request->document_primary;
            $client->zip_code = $request->zip_code;
            $client->address = $request->address;
            $client->address_number = $request->address_number;
            $client->district = $request->district;
            $client->city = $request->city;
            $client->state = $request->state;
            $client->is_active = $request->is_active;
            $client->save();
            return [
                "status" => true,
                "message" => "Cadastrado com sucesso"
            ];

        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao cadastrar cliente"
            ];
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //show one client
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message"=> "token inválido ou expirado"
            ];
        }
        try{
            $client = DB::selectOne("select * from _client where _id = '$id' or _id_crm='$id'");
            $client = Client::where("_id", "=", $id)->orWhere("_id_crm", "=", $id)->get();
            return [
                "status" => true,
                "client" => $client
            ];
        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao selecionar cliente"
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update client
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message"=> "token inválido ou expirado"
            ];
        }
        $validate = $request->validate([
            "_id_crm" => ["required", "string"],
            "business_name" => ["required", "string"],
            "fantasy_name" => ["required", "string"],
            "phone" => ["nullable", "string"],
            "phone2" => ["nullable", "string"],
            "document_primary" => ["nullable", "string"],
            "document_secundary" => ["nullable", "string"],
            "zip_code" => ["nullable", "string"],
            "address" => ["nullable", "string"],
            "address_number" => ["nullable", "string"],
            "district" => ["nullable", "string"],
            "city" => ["nullable", "string"],
            "state" => ["nullable", "string"],
            "is_active" => ["required", "boolean"],
        ]);
        if(!$validate){
            return [
                "status" => false,
                "message" => "campos obrigátorios não preenchidos"
            ];
        }
        try {
            $client = Client::find($id);
            $client->_id_crm = $request->_id_crm;
            $client->business_name = $request->business_name;
            $client->fantasy_name = $request->fantasy_name;
            $client->phone = $request->phone;
            $client->phone_2 = $request->phone_2;
            $client->document_primary = $request->document_primary;
            $client->zip_code = $request->zip_code;
            $client->address = $request->address;
            $client->address_number = $request->address_number;
            $client->district = $request->district;
            $client->city = $request->city;
            $client->state = $request->state;
            $client->is_active = $request->is_active;
            $client->save();
            
            return [
                "status" => true,
                "message" => "atualizado com sucesso"
            ];

        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao atualizar cliente"
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //destroy client
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message"=> "token inválido ou expirado"
            ];
        }
        try{
            DB::delete("delete from _client where _id = '$id'");
            return [
                "status" => true,
                "message" => "deletado com sucesso"
            ];
        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao deletar client"
            ];
        }
    }
}
