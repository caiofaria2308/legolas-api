<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;


class ProductController extends Controller
{
    /**
     * Decrypt token and return the data.
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function token_decrypt(Request $request){
        $token = $request->bearerToken(); 
        if($token == null){
            return [
                "status" => false, 
                "message" =>"token é obrigatório!"
            ];
        }
        $token_decrypt = Crypt::decrypt($token);
        $token = explode(',', $token_decrypt);        
        $authToken = $token[0];
        $userId = $token[1];
        $isadmin = $token[2];
        $header = [
            "X-Auth-Token" => $authToken,
            "X-User-Id" => $userId
        ];
        try{
            $url = 'https://chat.interativanet.com.br/api/v1/me';
            $response = Http::withHeaders($header)->get($url);
            $response = $response -> json();
            if($response["status"] != "error"){
                return [
                    "status" => true,
                    "authToken"=>$authToken,
                    "userId"=>$userId,
                    "isadmin"=>$isadmin
                ];
            }else{
                return [
                    "status" => false
                ];
            }
        }catch(Exception $e){
            return [
                "status" => false
            ];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Get all products
        $to = $this->token_decrypt($request);
        if(!$to["status"]){
            return [
                "status" => false, 
                "message"=> "token inválido ou expirado"
            ];
        }
        try{
            $products = Product::orderBy("name")->get();
            return [
                "status" => true, 
                "products" => $products
            ];
        }catch(Exception $e){
            return [
                "status" => false, 
                "message" => "Erro ao selecionar todos produtos"
            ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create product
        $to = $this->token_decrypt($request);
        if(!$to["status"]){
            return [
                "status" =>false, 
                "message"=>"token inválido ou expirado"
            ];
        }
        $validated = $request -> validate([
            'name' => ['required', 'string']
        ]);
        
        try{
            $product = new Product;
            $product->name = $request->name;
            $product->save();
            return [
                "status" =>true, 
                "message" => "Produto cadastrado com sucesso!"
            ];
        }catch(Exception $e){
            return [
                "status" =>false, 
                "message" => "Erro ao cadastrar produto"
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //Get product by id
        $to = $this->token_decrypt($request);
        if(!$to["status"]){
            return [
                "status" => false, 
                "message"=> "token inválido ou expirado"
            ];
        }
        try{
            $products = Product::find($id);
            return [
                "status" => true, 
                "product" => $products
            ];
        }catch(Exception $e){
            return [
                "status" => false, 
                "message" => "Erro ao selecionar produto"
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update product by id 
        $to = $this->token_decrypt($request);
        if(!$to["status"]){
            return [
                "status" => false, 
                "message"=>"token inválido ou expirado"
            ];
        }
        $validated = $request -> validate([
            'name' => ['required', 'string'],
            'is_active' => ['required', 'boolean']
        ]);
        if(!$validated){
            return [
                "status" => false, 
                "message" => "Campos obrigatórios não preenchidos!"
            ];
        }
        
        try{
            $product = Product::find($id);
            $product->name = $request->name;
            $product->is_admin = $request->is_admin;
            $product->save();
            return [
                "status" => true, 
                "message" => "Produto atualizado com sucesso!"
            ];
        }catch(Exception $e){
            return [
                "status" => false, 
                "message" => "erro ao atualizar produto"
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $to = $this->token_decrypt($request);
        if(!$to["status"]){
            return [
                "status" => false, 
                "message"=> "token inválido ou expirado"
            ];
        }
        try{
            DB::delete('delete from _product where _id = ? ', [$id]);
            return [
                "status" => true, 
                "message" => "Deletado com sucesso!"
            ];
        }catch(Exception $e){
            return [
                "status" => false, 
                "message" => "erro ao deletar produto"
            ];
        }
    }
}
