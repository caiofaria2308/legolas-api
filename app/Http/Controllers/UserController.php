<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Decrypt token and return the data.
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function token_decrypt(Request $request){
        $token = $request->bearerToken(); 
        if($token == null){
            return [
                "status" => false, 
                "message" =>"token é obrigatório!"
            ];
        }
        $token_decrypt = Crypt::decrypt($token);
        $token = explode(',', $token_decrypt);        
        $authToken = $token[0];
        $userId = $token[1];
        $isadmin = $token[2];
        $header = [
            "X-Auth-Token" => $authToken,
            "X-User-Id" => $userId
        ];
        try{
            $url = 'https://chat.interativanet.com.br/api/v1/me';
            $response = Http::withHeaders($header)->get($url);
            $response = $response -> json();
            if($response["status"] != "error"){
                return [
                    "status" => true,
                    "authToken"=>$authToken,
                    "userId"=>$userId,
                    "isadmin"=>$isadmin
                ];
            }else{
                return [
                    "status" => false
                ];
            }
            return [
                "status" => true,
            ];
        }catch(Exception $e){
            return [
                "status" => false
            ];
        }
    }
    
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function index(Request $request)
    {
       /**
         * Decrypt token and return the data.
         * @param \Illuminate\Http\Request  $request
         * @return array
        */ 
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" =>false, 
                "message"=>"token inválido ou expirado"
            ];
        }
        try{
            $response = User::orderBy('name')->get();
            return [
                "status" =>true, 
                'users'=>$response
            ];
        }catch(Exception $e){
            return [
                "status" =>false, 
                "message"=>"erro ao selecionar todos usuários"
            ];
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {   
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" =>false, 
                "message"=>"token inválido ou expirado"
            ];
        }
        try{
            $response = User::find($id);
            return [
                "status" =>true, 
                "user"=> $response
            ];
        }catch (Exception $e){
            return[
                "status" =>false, 
                "message"=>"erro ao selecionar usuário"
            ];
        }
    }

    /**

     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request -> validate([
            'name' => ['required', 'string'],
            'is_admin' => ['required', 'bool']
        ]);
        if(!$validated){
            return [
                "status" =>false, 
                "message" => "Campos obrigatórios não preenchidos!"
            ];
        }  
        
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" =>false, 
                "message"=>"token inválido ou expirado"
            ];
        }else if($token["userId"] != $id){
            return [
                "status" =>false, 
                "message"=>"você não tem autorização pra fazer isso"
            ];
        }
        try{
            #DB::update("update _user set name = ?, is_admin = ?, updated_date = current_timestamp() where _id='$id'", [$request->name, $request->is_admin]);
            $user = User::find($id);
            $user->name = $request->name;
            $user->is_admin = $request->is_admin;
            $user->save();
            return[
                "status" =>true, 
                "message" => "usuário atualizado com sucesso"
            ];
        }catch(Exception $e){
            return [
                "status" => true,
                "message" => "erro ao atualizar usuário"
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" =>false, 
                "message"=>"token inválido ou expirado"
            ];
        }else if($token["userId"] != $id){
            return [
                "status" =>false, 
                "message"=>"você não tem autorização pra fazer isso"
            ];
        }
        try{
            DB::delete("delete from _user where _id = '$id'");
            return[
                "status" =>true, 
                "message" => "deletado com sucesso"
            ];
        }catch(Exception $e){
            return[
                "status" =>false, 
                "message" => "erro ao deletar usuário"
            ];
        }
    }

    /**
     * Login.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        //Login and return token
        $validated = $request -> validate([
            'username' => ['required', 'string'],
            'password' => ['required', 'string']
        ]);
        if(!$validated){
            return ["message" => "Campos obrigatórios não preenchidos!"];
        }
        try{
            $response = Http::acceptJson()->post('https://chat.interativanet.com.br/api/v1/login', [
                "username" => $request->username,
                "password" => $request->password
            ]);
            $json = $response->json();
            if($json["status"] != "success"){
                return ["message"=> "Username/password incorrect", "error" => $json];
            }
            
            $json = $json["data"];
            $user = User::find($json["userId"]);
            if($user->count() == 0){
                $user = new User;
            }
            $user->_id = $json["userId"];
            $user->name = $json["me"]["name"];
            $user->save();
            $authToken = $json["authToken"];
            $userId = $json["userId"];
            $user = User::find($userId);
            $is_admin = $user->is_admin;

            $token = "$authToken,$userId, $is_admin";
            return ["status"=>true, "token" => Crypt::encrypt($token)];
        }catch(Exception $e){
            return ["status"=>false ,"message" => "Erro ao fazer login"];
        }
    }

    public function logout(Request $request){
        $url = 'https://chat.interativanet.com.br/api/v1/logout';
        $token = $this -> token_decrypt($request);
        $header = [
            "X-Auth-Token" => $token["authToken"],
            "X-User-Id" => $token["userId"]
        ];
        $response = Http::withHeaders($header)->get($url);
        $response = $response -> json();
        if($response["status"] == "success"){
            return [
                "status"=> true,
                "message" => "Logout feito com sucesso"
            ];
        }else{
            return [
                "status" => false,
                "message"=>"erro ao fazer logout"
            ];
        }
    }

    /**
     * Verify token
     * @param string token
     * @return \Illuminate\Http\Response
     */

     public function verify_login(Request $request){
        $url = 'https://chat.interativanet.com.br/api/v1/me';
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=>"token inválido ou expirado"];
        }
        $header = [
            "X-Auth-Token" => $token["authToken"],
            "X-User-Id" => $token["userId"]
        ];
        $response = Http::withHeaders($header)->get($url);
        $response = $response -> json();
        if($response["status"] != "error"){
            return [
                "status" => true,
                "user" => [
                    '_id' => $response["_id"],
                    "username" => $response["username"],
                    "name" => $response["name"],
                    "statusLivechat" => $response["statusLivechat"]
                ]
            ];
        }else{
            return [
                "status" =>false,
                "message" => "Você precisa estar logado para fazer isso"
            ];
        }
     }
}
