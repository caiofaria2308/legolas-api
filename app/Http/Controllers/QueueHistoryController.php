<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\QueueHistory;
use Exception;

class QueueHistoryController extends Controller
{
    /**
     * Decrypt token and return the data.
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function token_decrypt(Request $request){
        $token = $request->bearerToken(); 
        if($token == null){
            return [
                "status" => false, 
                "message" =>"token é obrigatório!"
            ];
        }
        $token_decrypt = Crypt::decrypt($token);
        $token = explode(',', $token_decrypt);        
        $authToken = $token[0];
        $userId = $token[1];
        $isadmin = $token[2];
        $header = [
            "X-Auth-Token" => $authToken,
            "X-User-Id" => $userId
        ];
        try{
            $url = 'https://chat.interativanet.com.br/api/v1/me';
            $response = Http::withHeaders($header)->get($url);
            $response = $response -> json();
            if($response["status"] != "error"){
                return [
                    "status" => true,
                    "authToken"=>$authToken,
                    "userId"=>$userId,
                    "isadmin"=>$isadmin
                ];
            }else{
                return [
                    "status" => false
                ];
            }
        }catch(Exception $e){
            return [
                "status" => false
            ];
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get all history of queue
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        $validate = $request->validate([
            "_queue" => ["string", 'required']
        ]);
        if(!$validate){
            return [
                "status" =>false, 
                "message" => "Campos obrigatórios não preenchidos!"
            ];
        }  
        try{
            $queues = QueueHistory::orderBy('created_date desc')->where("_queue", $request->_queue)->get();
            return [
                "status"=>true,
                "queue_history"=>$queues
            ];
        }catch(Exception $e){
            return[
                "status" => false,
                "message" =>"Erro ao selecionar histórico da fila"
            ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store queue history
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        $validate = $request->validate([
            "_queue" => ["string", 'required'],
            "_client" => ["string", "required"],
            "history_date" => ["string", "required"],
            "title" => ["string", "optional"],
            "description" =>["string", "optional"],
            "is_finished" => ["bool", "required"]
        ]);
        if(!$validate){
            return [
                "status" =>false, 
                "message" => "Campos obrigatórios não preenchidos!"
            ];
        }  
        try{
            $queue = new QueueHistory;
            $queue->_queue = $request->_queue;
            $queue->_client = $request->_client;
            $queue->_user = $token["userId"];
            $queue->history_date = $request->history_date;
            $queue->title = $request->title;
            $queue->description = $request->description;
            $queue->is_finished = $request->is_finished;
            $queue->save();
            return [
                "status" => true,
                "message" => "cadastrado com sucesso"
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message"=>"erro ao criar histórico da fila"
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //show one queue history
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        try{
            $queue = QueueHistory::find($id);
            return [
                "status" => false,
                "queue_history" => $queue
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao selecionar histórico da fila!"
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update queue history
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        $validate = $request->validate([
            "_queue" => ["string", 'required'],
            "_client" => ["string", "required"],
            "history_date" => ["string", "required"],
            "title" => ["string", "optional"],
            "description" =>["string", "optional"],
            "is_finished" => ["bool", "required"]
        ]);
        if(!$validate){
            return [
                "status" =>false, 
                "message" => "Campos obrigatórios não preenchidos!"
            ];
        }  
        try{
            $queue = QueueHistory::find($id);
            if($token["userId"] != $queue->_user && !$token["is_admin"]){
                return [
                    "status" => false,
                    "message" =>"você não tem autorização para atualizar esse histórico"
                ];
            }
            $queue->_queue = $request->_queue;
            $queue->_client = $request->_client;
            $queue->history_date = $request->history_date;
            $queue->title = $request->title;
            $queue->description = $request->description;
            $queue->is_finished = $request->is_finished;
            $queue->save();
            return [
                "status" => true,
                "message" => "atualizado com sucesso"
            ];
            
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao atualizar histórico da fila"
            ];
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //destroy queue history
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        try{
            $queue = QueueHistory::find($id);
            if($token["userId"] != $queue->_user && !$token["is_admin"]){
                return [
                    "status" => false,
                    "message" =>"você não tem autorização para deletar esse histórico"
                ];
            }
            DB::delete("delete from _queue_history where _id = '$id'");
            return [
                "status" => true,
                "message" => "deletado com sucesso"
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao deletar histórico da visita"
            ];
        }
    }
}
