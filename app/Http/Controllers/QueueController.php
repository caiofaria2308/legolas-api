<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\Queue;

class QueueController extends Controller
{
    /**
     * Decrypt token and return the data.
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function token_decrypt(Request $request){
        $token = $request->bearerToken(); 
        if($token == null){
            return [
                "status" => false, 
                "message" =>"token é obrigatório!"
            ];
        }
        $token_decrypt = Crypt::decrypt($token);
        $token = explode(',', $token_decrypt);        
        $authToken = $token[0];
        $userId = $token[1];
        $isadmin = $token[2];
        $header = [
            "X-Auth-Token" => $authToken,
            "X-User-Id" => $userId
        ];
        try{
            $url = 'https://chat.interativanet.com.br/api/v1/me';
            $response = Http::withHeaders($header)->get($url);
            $response = $response -> json();
            if($response["status"] != "error"){
                return [
                    "status" => true,
                    "authToken"=>$authToken,
                    "userId"=>$userId,
                    "isadmin"=>$isadmin
                ];
            }else{
                return [
                    "status" => false
                ];
            }
        }catch(Exception $e){
            return [
                "status" => false
            ];
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //show all queue
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message" => "token inválido ou inexistente"
            ];
        }
        try{
            $queues = Queue::orderBy('created_date desc')->get();
            return [
                "status" => true,
                "queues" => $queues
            ];
        }catch(Exception){
            return [
                "status" => false,
                "message" =>"erro ao selecionar filas"
            ];
        }
    }
     /*
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create new queue
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message" => "token inválido ou inexistente"
            ];
        }
        $validate = $request->validate([
            "name" => ["required", "string"],
            "_product" => ["required", "string"],
            "description" => ["nullable", "string"],
            "is_active" => ["required", "boolean"],
        ]);
        if(!$validate){
            return [
                "status" => false,
                "message" => "campos obrigatórios não foram preenchidos"
            ];
        }
        try{
            $queue = new Queue;
            $queue->name = $request->name;
            $queue->_product = $request->_product;
            $queue->description = $request->description;
            $queue->is_active = $request->is_active;
            $queue->save();
            return [
                "status" => true,
                "message" => "cadastrado com sucesso"
            ];
        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao cadastrar fila"
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //show one queue 
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message" => "token inválido ou inexistente"
            ];
        }
        try{
        $queue = Queue::find($id);
        return [
            "status" => true,
            "queue" => $queue
        ];
        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao selecionar fila por ID"
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //updateq queue
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message" => "token inválido ou inexistente"
            ];
        }
        $validate = $request->validate([
            "name" => ["required", "string"],
            "_product" => ["required", "string"],
            "description" => ["nullable", "string"],
            "is_active" => ["required", "boolean"],
        ]);
        if(!$validate){
            return [
                "status" => false,
                "message" => "campos obrigatórios não foram preenchidos"
            ];
        }
        try{
            $queue = Queue::find($id);
            $queue->name = $request->name;
            $queue->_product = $request->product;
            $queue->description = $request->description;
            $queue->is_active = $request->is_active;
            $queue->save();
            return [
                "status" => true,
                "message" => "atualizado com sucesso"
            ];
        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao atualizar fila"
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //destroy queue
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return [
                "status" => false,
                "message" => "token inválido ou inexistente"
            ];
        }
        try{
            DB::delete("delete from _queue where _id = '$id'");
            return [
                "status" => true,
                "message" => "deletado com sucesso"
            ];
        }catch(Exception){
            return [
                "status" => false,
                "message" => "erro ao deletar fila"
            ];
        }
    }
}
