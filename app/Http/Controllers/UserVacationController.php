<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\UserVacation;

class UserVacationController extends Controller
{
    /**
     * Decrypt token and return the data.
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function token_decrypt(Request $request){
        $token = $request->bearerToken(); 
        if($token == null){
            return [
                "status" => false, 
                "message" =>"token é obrigatório!"
            ];
        }
        $token_decrypt = Crypt::decrypt($token);
        $token = explode(',', $token_decrypt);        
        $authToken = $token[0];
        $userId = $token[1];
        $isadmin = $token[2];
        $header = [
            "X-Auth-Token" => $authToken,
            "X-User-Id" => $userId
        ];
        try{
            $url = 'https://chat.interativanet.com.br/api/v1/me';
            $response = Http::withHeaders($header)->get($url);
            $response = $response -> json();
            if($response["status"] != "error"){
                return [
                    "status" => true,
                    "authToken"=>$authToken,
                    "userId"=>$userId,
                    "isadmin"=>$isadmin
                ];
            }else{
                return [
                    "status" => false
                ];
            }
        }catch(Exception $e){
            return [
                "status" => false
            ];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return all users in vacation
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["status" => False, "message"=> "token inválido ou expirado"];
        }
        try{
            $vacations = UserVacation::orderBy("finish_date", "desc")->get();

            return [
                "status" =>true, 
                "vacations" => $vacations
            ];
        }catch(Exception $e){
            return[
                "status" =>false, 
                "message" => "erro ao selecionar todas férias"
            ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store new vacation
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        $validate = $request->validate([
            '_user' =>['required', 'string'],
            'start_date' => ['required', 'string'],
            'finish_date' => ['required', 'string']
        ]);
        if(!$validate){
            return [
                "status" => false,
                "message" => "Todos os campos são obrigatórios!"
            ];
        }
        try{
            $vacation = new UserVacation;
            $vacation->_user = $request->_user;
            $vacation->start_date = $request->start_date;
            $vacation ->finish_date = $request->finish_date;
            $vacation->save();
        }catch(Exception $e){
            return [
                "status" =>false, 
                "message" => "erro ao cadastrar férias"
            ];
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //show vacation by id
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        try{
            $vacation = UserVacation::find($id);
            return [
                "status" => true,
                "vacation" => $vacation
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao selecionar férias"
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update vacation
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        $validate = $request->validate([
            "_user" => ["required", 'string'],
            "start_date" => ['required', 'string'],
            "finish_date" => ['required', 'string']
        ]);
        if(!$validate){
            return [
                "status" => false,
                "message" => "Campos obrigatórios incompletos"
            ];
        }
        if($request->_user != $token["userId"] && !$token["is_admin"]){
            return [
                "status" => false, 
                "message" => "você não tem autorização pra fazer isso"
            ];
        }
        try{
            $vacation = UserVacation::find($id);
            $vacation->_user = $request->_user;
            $vacation->start_date = $request->start_date;
            $vacation ->finish_date = $request->finish_date;
            $vacation->save();
            return [
                "status" => true,
                "message" => "atualizado com sucesso"
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao atualizar férias"
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //destroy vacation
        $token = $this->token_decrypt($request);
        if(!$token["status"]){
            return ["message"=> "token inválido ou expirado"];
        }
        if($request->_user != $token["userId"] && !$token["is_admin"]){
            return [
                "status" => false, 
                "message" => "você não tem autorização pra fazer isso"
            ];
        }
        try{
            DB::delete("delete from _user_vacation where _id = '$id'");
            return [
                "status" => true,
                "message" =>"deletado com sucesso"
            ];
        }catch(Exception $e){
            return [
                "status" => false,
                "message" => "erro ao deletar férias"
            ];
        }

    }
}
