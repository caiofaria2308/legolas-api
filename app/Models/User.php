<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.Client
     * @var array
     */


    protected $table = "_user";

    protected $primaryKey = "_id";
    protected $keyType = 'string';
    public $incrementing = false;

    public $timestamp = true;
    const CREATED_AT = "created_date";
    const UPDATED_AT = "updated_date";

}
