<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class QueueHistory extends Model
{
    use HasFactory;

    protected $table = "_queue_history";

    protected $primaryKey = "_id";
    protected $keyType = 'string';
    public $incrementing = false;

    public $timestamp = true;
    const CREATED_AT = "created_date";
    const UPDATED_AT = "updated_date";
}
